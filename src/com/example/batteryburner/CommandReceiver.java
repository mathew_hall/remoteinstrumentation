package com.example.batteryburner;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.util.Log;

import com.example.batteryburner.tests.*;
import com.example.batteryburner.tests.Commands.UIInteraction;
public class CommandReceiver {

	
	private int port;
	private ServerSocket socket;
	private PrintWriter output;
	private Socket clientSocket;
	public int getPort() {
		return port;
	}

	
	
	public CommandReceiver(int port) throws IOException{
		this.port = port;
		socket = new ServerSocket(port);
	}
	
	
	public UIInteraction getCommand() throws IOException{
		clientSocket = socket.accept();
		InputStream isr = clientSocket.getInputStream();
		output = new PrintWriter(clientSocket.getOutputStream(),true);
		return Commands.UIInteraction.parseDelimitedFrom(isr);
	}
	
	public void sendResponse(String resp){
		if(output != null){
			output.println(resp);
			output.flush();
			Log.i("ouptut", resp);
		}else{
			Log.i("output","output is null");
		}
	}
	
	public void fail(String message){
		if(output != null){
			output.println("FAILURE");
			output.println("MESSAGE\t" + message);
		}
	}
	
	public void close(){
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public void endResponse() {
		try {
			clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
