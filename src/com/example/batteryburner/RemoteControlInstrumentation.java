package com.example.batteryburner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.example.batteryburner.tests.Commands.UIInteraction;
import com.example.batteryburner.tests.Commands.UIInteraction.Type;
import com.jayway.android.robotium.solo.RobotiumUtils;
import com.jayway.android.robotium.solo.Solo;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.test.TouchUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class RemoteControlInstrumentation extends Instrumentation{
	Activity activity;
	Solo solo;
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);

        // When this instrumentation is created, we simply want to start
        // its test code off in a separate thread, which will call back
        // to us in onStart().
        start();
    }
    
	@SuppressLint("NewApi")
	public void onStart(){
		super.onStart();
		
		
		
//Note: solo must be created BEFORE the activity starts for things to work properly.
        solo= new Solo(this);
        
        
        // First start the activity we are instrumenting -- the contacts
        // list.
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName(getTargetContext(),
                "org.wordpress.android.ui.posts.PostsActivity");
        activity = startActivitySync(intent);
        
        
        
        
        
        activity = solo.getCurrentActivity();
        List<View> views = solo.getViews();
        for(View v : views){
        	Log.i("VIEWS", v.toString());
        }
        
        try {
			CommandReceiver receiver = new CommandReceiver(4444);
			UIInteraction cmd = null;
			while((cmd = receiver.getCommand()) != null){
				activity = solo.getCurrentActivity();
				Log.v("COMM", "Got cmd " + cmd);
				
				try{
				
				switch(cmd.getType()){
				case PRESS:
					pressElement(cmd.getId());
					break;
				case TEXT:
					enterText(cmd.getId(), cmd.getText());
					break;
				case SPINNER:
					openSpinnerSelectItem(cmd.getId(), cmd.getItem());
					break;
				case GETACTIVITY:
					Log.i("SKT", "Activity query");
					receiver.sendResponse("Activity=" + solo.getCurrentActivity().getClass().getName());
					break;
				case GETVIEWS:
					Log.i("SKT", "Views");
					views = solo.getViews();
					for(View v : views){
						receiver.sendResponse(v.toString());
					}
					
					break;
				case GETCLICKABLE:
					Log.i("SKT","Clickable");
					views = solo.getViews();
					views = RobotiumUtils.removeInvisibleViews(views);
					
					
					for(View v : views){
						char isAbsListView = v instanceof AbsListView? 'G' : '.';
						
						
			        	receiver.sendResponse(v.toString() + " " + isAbsListView);
			        }
					break;
				case GETITEMS:
					ListView target = (ListView) activity.findViewById(cmd.getId());
					if(target == null){
						Log.w("GETITEMS", "Target was null for id" + cmd.getId());
						break;
					}
					
					
					int c = target.getChildCount();
					
					receiver.sendResponse("viewcount=" + c);
					break;
				case CLICKLISTITEM:
					final int item = cmd.getItem();
					final int viewId = cmd.getId();
					
					
					solo.clickInList(item,viewId);
					//solo.clickInList(item);
					/*runOnMainSync(new Runnable() {
						
						@Override
						public void run() {
							View v = list.getChildAt(item);
							if(v != null){
								v.performClick();	
							}else{
								Log.w("CLICKLISTITEM", "null list item for view id " + viewId +" item" + item);
							}
							
						}
					});*/
					
					
					break;
				case SENDKEY:
					solo.sendKey(cmd.getKeycode());
					break;
				case GETENTRYLIST:
					int listView = 0;
					int button = 0;
					
					int viewNum = 0;
					int spinnerNum = 0;
					int radioButtonNum =0;
					int checkBoxNum = 0;
					int editTextNum = 0;
					views = solo.getViews();
					views = RobotiumUtils.removeInvisibleViews(views);
				
					for(View v : views){
						StringBuilder b = new StringBuilder();
						if(v instanceof Button){
							b.append("B{" + button + "}");
							button ++;
						}
						if(v instanceof AbsListView){
							b.append("L{" + listView +"}");
							listView++;
						}
						if(v instanceof Spinner){
							b.append("S{" + spinnerNum + "}");
							spinnerNum++;
						}
						if(v instanceof RadioButton){
							b.append("R{" + radioButtonNum + "}");
							radioButtonNum++;
						}
						if(v instanceof CheckBox){
							b.append("C{" + checkBoxNum + "}");
							checkBoxNum++;
						}
						if(v instanceof EditText){
							b.append("T{"+ editTextNum + "}");
							editTextNum++;
						}
						b.append("V{" + viewNum + "}");
						receiver.sendResponse(v.getClass().getName() + "|" + v.getId() + "|" + (v.isEnabled()?'e':'.') + "|" + b.toString());
						
						viewNum++;
			        }
					break;
				case PRESSVIEW:
					views = solo.getViews();
					views = RobotiumUtils.removeInvisibleViews(views);
					
					if(views.size() <= cmd.getId()){
						receiver.sendResponse("INVALID ARGUMENT: Only " + views.size() + " views exist.");
						break;
					}
					
					if(cmd.getLongPress()){
						solo.clickLongOnView(views.get(cmd.getId()));
					}else{
						solo.clickOnView(views.get(cmd.getId()));
					}
					break;
					
				case GETVIEWSTATE:
					//TODO: need to determine if a dialog is visible or not basically.

					
				}
				}catch(Throwable e){
					receiver.fail(e.getMessage());
				}
				receiver.endResponse();
				
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("Comms","Excaption when listening on 4444", e);
		}
        
        
        
	}
	
	public void pressElement(int id){
		Log.i("CMD", "Pressing " + id);
		final View button1 = activity.findViewById(id);
		
		if(button1 == null){
			Log.i("Error", "Button was null for id" + id);
			return;
		}
		
		solo.clickOnView(button1);
		
	}
	
	
	public void enterText(int id, String text){
		Log.i("CMD", "Entering " + text + " in " + id);
		EditText target = null;
		List<View> views = solo.getViews();
		int i = 0;
		for(View v : views){
			if(v instanceof EditText){			
				if(i == id){
					target = (EditText)v;
					break;
				}
				i++;
			}
		}
		if(target == null){
			return;
		}
		solo.enterText(target, text);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //wait for text to be accepted by the field
//		final TextView tv = (TextView) activity.findViewById(id);
//		runOnMainSync(new Runnable() {
//			
//			@Override
//			public void run() {
//				tv.performClick();
//				tv.requestFocus();
//			}
//		});
//	
//		sendStringSync(text);
	}
	
	public void openSpinnerSelectItem(int id, final int itemNumber){
		Log.i("CMD", "Choosing item " + itemNumber + " from " + id);
		final Spinner sp = (Spinner)activity.findViewById(id);
		runOnMainSync(new Runnable() {
			
			@Override
			public void run() {
				sp.setSelection(itemNumber);
			}
		});
	}
	
	public void clickListViewItem(int id, final int itemNumber, final long itemId){
		final ListView lv = (ListView)activity.findViewById(id);
		runOnMainSync(new Runnable() {
			
			@Override
			public void run() {
				lv.performItemClick(lv, itemNumber,itemId );
			}
		});
		
	}

}

