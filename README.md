# Android Remote Control Instrumentation

This package defines an instrumentation that can be injected into an app. Set the package name in the `AndroidManifest.xml` to target an app. Note that it must be signed with the same key for this to work.

Start the target with `am instrument com.example.batteryburner.test/com.example.batteryburnder.RemoteControlInstrumentation`. It will expose Robotium via a `protobuf` interface.